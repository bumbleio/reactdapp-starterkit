import React, { Component } from 'react';
import './App.css';
import Web3 from 'web3';


class App extends Component {


  componentWillMount() {
    this.loadBlockchainData()
  }

async loadBlockchainData() {

  console.log("loaded12")
  console.log("network:")
  const web3 = new Web3(Web3.givenProvider || "http://localhost:8545")
  const network = await web3.eth.net.getNetworkType();
  const accounts = await web3.eth.getAccounts();
  console.log("network:", network)
  // web3.eth.getAccounts(console.log);
  //console.log("account:", accounts[0])
  this.setState({ account: accounts[0] })

}


constructor(props) {
  super(props)
  this.state = {
    account: ''
  }
}


  render() {
    return (
      <div>
        <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
          <a
            className="navbar-brand col-sm-3 col-md-2 mr-0"
            href="http://www.dappuniversity.com/bootcamp"
            target="_blank"
            rel="noopener noreferrer"
          >
            UFC Bet
          </a>
        </nav>
        <div className="container-fluid mt-5">
        <p>Your account is: {this.state.account}</p>
        </div>
      </div>
    );
  }
}

export default App;
